//LinkedList.cpp

#include "stdafx.h"

#include "LinkedList.hpp"

List::List(){

	root = NULL;						          //these three pointers are set to NULL in the ctor
	temp = NULL;
	current = NULL;

}

void List::push_front(int addData)
{


	Node* n = new Node;
	n->data = addData;

	if (root != NULL)							//if we already have data in the list..
	{
		temp = root;							//we save the previous first node

		n->next = root;							//new data's next is set to root, therefore we are placing it BEFORE the previous root

		root = n;								//new data is set as the root

		root->next = temp;						//we don't want to lose the previous first node, so we set our new root's next to the old root.

		std::cout << addData << " was added to the front of the list!\n";
	}

	else
	{
		root = n;

		std::cout << addData << " was added to the front of the list!\n";
	}


}

void List::push_back(int addData)		          //This function ADDS DATA to the BACK of an existing list.
{

	Node* n = new Node;
	n->next = NULL;						          //sets the next node that our new node is pointing to, to NULL

	n->data = addData; 

	if (root != NULL)					          //if root is pointing to smth that is not NULL, ie if we already have data in our list..
	{
		current = root;					          //then go to the node at the front of the list.

		while (current->next != NULL)	          //Keep going through the list as long as the current pointer is not at the end of the list (next node is NULL at the end of the list).
		{
			current = current->next;
		}
		current->next = n;				          //at the end of the list, set the last node to n, our new added value.
	}

	else

	{
		root = n;						          //if we already have a list, we can just add n at the front of the list, since front==back for a list with one value.
	}
}

void List::find(int findData)		          //This function FINDS and REMOVES DATA from the list (to prove it can find)
{

	Node* removePtr = NULL;
	current = root;
	temp = root;

	while (current != NULL &&			          //As long as we're not at the end of the list..
	 current->data != findData)			          //and we're not at the element we want to remove...
	{
		temp = current;					          //then traverse through the list.
		current = current->next;		          //temp stays at current and current goes to the next node at every step. Ie trailing.
	}

	if (current == NULL)				          //If we've gone through the list without finding the value we're looking for, then
	{
		std::cout << findData << " was not found in the list.\n";		          //print to console that the inputted value does not exist in the list.

		delete removePtr;				          //to prevent memleak!
	}

	else                                          //Else if we've found the value we wanted to delete
	{
		removePtr = current;			          //then we set the removal pointer to the value we want to delete
		current = current->next;		          //we move the current pointer over (we don't want the pointer ITSELF to be deleted!)
		temp->next = current;			          //join temp back up with current

		if (removePtr == root)			          //so the list doesn't lose root node if it's that's the value we want to delete
		{
			root = root->next;
			temp = NULL;
		}

		delete removePtr;				          //and delete the value!

		std::cout << findData << " was found and removed from the list!\n";

	}

}

void List::Print()
{
	current = root;

	while (current != NULL)				          //as long as we're not at the final node
	{
		std::cout << "   " << current->data << "   ";					//print out the current value
		current = current->next;										//advance the pointer to the next node
	}

	if (current == NULL)

	{
		std::cout << "   \n";
	}



}

void List::pop_front()							//REMOVE element from the front of the list
{
	current = root;							//we're going to remove the first node in the list

	root = current->next;							//the previously second node in the list will become our new root

	delete current;									//and delete

	std::cout << "The first node was removed from the list!\n";

};



void List::pop_back()							//""  back of the list
{

	current = root;										//start at the front of the list

	while (current->next->next != NULL)					//Keep going through the list as long as the current pointer is not at the end of the list (next node is NULL at the end of the list).
	{
		current = current->next;
	}

	delete current->next;								//once we're at the end of the list, delete the last node
	current->next = NULL;
	
	std::cout << "The last node was removed from the list!\n";
	

};

void List::clear()
{
	current = root;

	while (current->next->next != NULL)
	{
		current = current->next;
		delete current;
	}

	root = NULL;

	std::cout << "The list was cleared.\n";

}

void List::size()
{
	int listSize = 0;
	current = root;

	while (current)
	{
		listSize++;
		current = current->next;
	}
	std::cout << "The list currently has " << listSize << " nodes.\n";
}
