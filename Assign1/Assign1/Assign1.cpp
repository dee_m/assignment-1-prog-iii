// Assign1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "LinkedList.hpp"
#include "BinarySearchTree.hpp"
#include "unit.hpp"

int _tmain(int argc, _TCHAR* argv[])
{

	//LINKED LIST

	List test;

	test.push_back(8);			//create a new list, and push back the value 8
	test.push_back(3);
	test.push_back(1);
	test.push_back(17);

	test.Print();				//push back test

	test.size();				//size test

	test.push_front(6);			//push front test. FEEL FREE TO CHANGE THESE VALUES TO TEST THE FUNCTIONS.
	test.Print();

	test.size();
	test.Print();

	test.find(1);
	test.Print();				//find and remove test

	test.find(4);
	test.Print();				//find and remove test pt 2

	test.pop_front();
	test.Print();

	test.pop_back();
	test.Print();

	test.clear();
	test.Print();

	//BINARY SEARCH TREE

	BinSearchTree<int> *BST = new BinSearchTree < int >;
	std::cout << "\n BinarySearchTree!\n";
	BST->insert(5);
	BST->insert(13);
	BST->insert(3);
	BST->insert(1);
	BST->insert(4);
	BST->insert(9);
	BST->insert(16);

	verify<int>(7, BST->size(), "Size of tree()");

	verify<int>(true, BST->find(1), "Find()");

	std::cout << "Traversal Pre-Order: ";
	BST->trav_pre_order();

	std::cout << "Traversal In-Order: ";
	BST->trav_in_order();

	std::cout << "Traversal Post-Order: ";
	BST->trav_post_order();

	BST->clear();
	verify<int>(0, BST->size(), "Clear() ");

	return 0;


}