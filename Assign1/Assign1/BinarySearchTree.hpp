//BinarySearchTree.cpp

//Binary Search Trees are similar to Linked Lists, but each node holds an inputted data and two references. 
//        5
//       / \
//      3   13
//    / |    | \
//   1  4    9  16
//Their structure means they are essentially twice as fast to search through!
//This makes BSTs very useful when making search functions!
//The downsides of BSTs are it's a bit more difficult to replace/remove datas, datas have to be organizable in a binary system
//A BTS with only datas on one side is essentially a Linked List. We don't want this, so we BALANCE BSTs out.

#include "stdafx.h"

template <typename T>
struct Node
{

	Node<T>* GreaterR = nullptr;		//Nodes that contain data GREATER than that of their parent node go to the RIGHT side of the tree
	Node<T>* LesserL = nullptr;			//Nodes that contain data LESSER than that of their parent node go to LEFT side of the tree
	T data;								//the data of a node

};

template <class T>
class BinSearchTree
{

private:

	Node<T>* root;										//The head node for the tree, ie the root. (The "topmost" node)


public:

	BinSearchTree()						//Ctor
	{
		root = nullptr;
	}

	~BinSearchTree()					//Dtor
	{
		Clear();
	}


	//INSERT FUNCTION
	void insert(T val)
	{
		Node<T> *current = root;						//start at root

		if (root == nullptr)							//if tree is empty
		{
			root = new Node<T>;							//make a new node
			root->data = val;							//and save our inserted value in the root node

			return;
		}

		while (current != nullptr)						//if the tree is NOT empty and we're not at the end of the list yet
		{

			if (val == current->data)					//if the value we want to insert is the same as the current node

				return;									//exit

			else if (val > current->data)				//if the value we want to insert is GREATER than the current node, we want to move to the right
			{

				if (current->GreaterR == nullptr)		//if there is no child on the right side of current node, we'll make a new one and insert the new value here
				{
					current->GreaterR = new Node<T>;
					current->GreaterR->data = val;
				}

				else									//if there is a child on the right side
				{
					current = current->GreaterR;		//we insert the new value here
				}

			}

			else										//if the value we want to insert is LESSER in than the current node, we want to move to the left
			{

				if (current->LesserL == nullptr)		//as previous
				{
					current->LesserL = new Node<T>;
					current->LesserL->data = val;
				}

				else									//as previous
				{
					current = current->LesserL;
				}

			}

		}

	}


	//CLEAR FUNCTION
	void clear()
	{
		clear(root);									//Call the function below and
		root = nullptr;									//clear the root node
	}

	void clear(Node<T>* node)
	{
		if (node == nullptr)							//if the tree is empty
			return;										//no need to clear

		clear(node->GreaterR);							//run the function recursively through the righthandside
		clear(node->LesserL);							//""									   lefthandside
		node = nullptr;									
		delete node;
	}




	//FIND FUNCTION
	bool find(T val)
	{
		Node<T>* current;								//start at the top root of the tree
		current = root;

		if (root == nullptr)							//if the list is empty just return false ie data can't possibly be in empty lisssttt
			return false;

		else if (findNode(val, current))				//if the list has data, run the findNode function
			return true;

		else
			return false;

	}

	bool findNode(T val, Node<T>* node)					//findNode function
	{

		if (val == node->data)							//if the value we're looking for is the same as the data of the node we're on
			return true;								//success! these ARE the droids we're looking for!

		else if (val < node->data)						//if the val we're looking for is smaller than the data of the node we're on, we're going to move to the LEFT child
		{

			if (node->LesserL == nullptr)				//if we're at the end of the lesser children, we did not find the value. So it's not in the list, return false.	
				return false;

			else										//else, if we're not at the end of the lesser children, keep going down the lefthand side
			{
				node = node->LesserL;					//move the cursor down the the lesser child
				findNode(val, node);					//recursion, run the function again to keep going 
			}

		}

		else if (val > node->data)						//if the value we're looking for is larger than the node we're on, we're going down the RIGHT
		{
			if (node->GreaterR == nullptr)				//as previous
				return false;

			else										//as previous. Recursion so we keep going down the righthand side aka through the greater children.
			{
				node = node->GreaterR;
				findNode(val, node);
			}
		}
	}



	//TRAVERSAL PRE-ORDER FUNCTION

		//This function displays the Root node's data, 
		//traverses the LEFT subtree by recursively calling the pre-order function, 
		//and then traverses the RIGHT subtree by doing the same.

	void trav_pre_order()
	{
		Node<T>* current = root;						//start at the root

		if (root)										//if we have data in the tree,
		{
			trav_pre_order(root);						//call function below to continue
		}

		std::cout << std::endl;
	}

	void trav_pre_order(Node<T>* current)
	{

		std::cout << current->data << " ";				//print out the data of EACH NODE prior to traversing. This way the Root node's data is listed first.

		if (current->LesserL)							//If we HAVE a lesser child (lefthandside)
		{
			trav_pre_order(current->LesserL);			//then call this function using this lesser child as the starting node. Recursion, to keep traversing as long as we have left children.
		}

		if (current->GreaterR)							//If we have a greater child (righthandside)
		{
			trav_pre_order(current->GreaterR);			//as previous
		}
	}


	//TRAVERSAL IN-ORDER
		//This function traverses the left subtree, 
		//THEN displays the data
		//THEN traverses the right subtree. This was, we retrieve data in a "sorted" order.

	void trav_in_order()
	{
		Node<T>* current = root;						//as previous

		if (root)
		{
			trav_in_order(current);
		}

		std::cout << std::endl;
	}

	void trav_in_order(Node<T>* current)				//If we have a lesser child, recursively call the function to keep traversing the list
	{
		if (current->LesserL)
		{
			trav_in_order(current->LesserL);
		}

		std::cout << current->data << " ";				//print out the node's data when we're done traversing the lefthand-side

		if (current->GreaterR)							//and now, if we have a greater child, recursively call the function to keep traversing
		{
			trav_in_order(current->GreaterR);
		}

	}



	//TRAVERSAL POST-ORDER FUNCTION
		//This function traverses the LEFT subtree with recursion
		//then traverses the RIGHT subtree with recursion
		//THEN displays the root data

	void trav_post_order()
	{
		Node<T>* current = root;						//as previous
		if (root)
		{
			trav_post_order(current);
		}

		std::cout << std::endl;
	}

	void trav_post_order(Node<T>* current)
	{

		if (current->LesserL)							//as previous
		{
			trav_post_order(current->LesserL);
		}

		if (current->GreaterR)							//as previous
		{
			trav_post_order(current->GreaterR);
		}

		std::cout << current->data << " ";				//and print out node data once we are done traversing the left and righthand children. 
	}


	//SIZE
	int size()
	{
		int size = 0;
		sizeoftree(root, size);							//call the function below
		return size;
	}

	void sizeoftree(Node<T>* node, int &size)
	{
		if (node != nullptr)							//if we're not at the end of the tree/if the tree has data
		{
			size++;										//increment
			sizeoftree(node->GreaterR, size);			//recursively call this function going down the righthandside of the tree
			sizeoftree(node->LesserL, size);			//recursively call this function going down the lefthandside of the tree
		}
	}

};