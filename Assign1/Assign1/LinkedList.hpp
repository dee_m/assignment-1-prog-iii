//LinkedList.hpp

#include "stdafx.h"


class List{

private:

	typedef struct Node
	{
		int data;					          //this is where the data for each node is stored
		Node* next;					          //this is the reference to the next node
	};

	Node* root;						          // for the first node, first pos in the list
	Node* temp;						          // for holding a temp node while swapping etc
	Node* current;					          // for the current node we're on, so we know our position in the list


public:

	List();							          //Ctor

	void push_front(int addData);	          //ADD element to the front of the list
	void push_back(int addData);	          //""				 back of the list

	void pop_front();					      //REMOVE element from the front of the list
	void pop_back();				          //""					  back of the list

	void clear();					          //Clears list

	void find(int findData);				  //Finds (and deletes) data in the list 

	void size();						      //Returns the length of the list, ie the number of nodes in the list

	void Print();					          //Print list

};